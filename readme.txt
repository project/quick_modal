= Quick Modal =
Adds an easily configured JQuery controlled modal for Drupal 7 to use as an alert. Messages can be customized using a limited set of HTML tags. Custom background 
images for desktop and mobile can be also be set. Module can be configured to display the message on internal pages as well as the front page, along with a number of
other features.

== Included Images == 
Mobile and desktop images by Paweł Czerwiński and Volodymyr Hryshchenko. Downloaded from Unsplash. Drupal icon sourced directly from drupal.org.

== Development notes == 
* JQuery uses fadeToggle(), which was introduced for 1.4.4. This is the same version vanilla drupal 7 ships with. So JQuery Update should not be a requirement. Maybe?

== ToDo ==
Ready for final testing pre-release...

= History =

== 7.x-1.0-beta ==
* let's release this
* removed version info from info file so we can promote on drupal.org

== 7.x-0.12 ==
* improved js handling of clickable background (not a new feature)

== 7.x-0.11 ==
* this version should feature lock the module before beta release
* updating / fixing instructions and field descriptions and documentation
* cleaning out unneeded comments and debug code
* added title class and drupal logo image for use with Modal Message default / sample content
* reduced "background as close button" timeout from 5 seconds to 3 seconds
* added simple JS to not show modal if there is an admin overlay being displayed

== 7.x-0.10 ==
* added session cookie based limit configuration feature
* orginized the config page a little
* added some inline config value validation
* ui typo corrections

== 7.x-0.9 ==
* updated interior pages field to delinate on a new line and support wildcards (more 'drupal like' the code)
* add basic drupal styled commenting and notes
* spelling & typo corrections & some code cleanup

== 7.x-0.8.1 ==
* correct version number in info

== 7.x-0.8 ==
* removed debugging info that slipped into the internal 0.7.1 build
* added "semi-transparent viewport background as close button" feature
* added custom inline styles feature

== 7.x-0.7.1 ==
* added img to allowed tag list

== 7.x-0.7 ==
* added option to display modal without background image
* added option for custom classes to be applied to the modal window (quick-modal-modal-window)
* set Display Modal on Front Page to un-checked (to give people a chance to configuring the modal before displaying).

== 7.x-0.6 ==
* changed module name and related identifiers from virus_popup to quick_modal
* cleaned JS and CSS
* updated default message variable to be less covid-19 related
* updated default background images to be less covid-19 related
* added config path variable to the info file

== 7.x-0.5 ==
* updated help document
* style tweaks
* switch from fadeIn() and fadeOut() to fadeToggle() to deal with the "TypeError: $.browser is undefined" on Jquery Update 1.9
* updated drupal_add_js to ensure it is loaded as close to last in the stack as possible

== 7.x-0.4 ==
* add method to change background image for mobile & desktop

== 7.x-0.3 ==
* add turn-on / turn-off feature (a checkbox select to show on front page)
* add method to include internal paths to display the modal
* updated help documentation

== 7.x-0.2 ==
* updated help document
* added custom permissions
* moved the modal content from an inline string construction to stand alone variable
* updated info file

== 7.x-0.1 ==
* added help document
* added filter for display of user input in modal (with limited HTML tags)

== 7.x-0.1-alpha: ==
* first release