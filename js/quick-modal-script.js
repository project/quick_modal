(function ($) {
			
	$(document).ready(function() {
		
		var noOverlay = true;
	
		if ($('#overlay-container').length) {
			noOverlay = false;
		}
	
		// modal window
		
		var isClickable = $('.quick-modal-front-modal').data('clickable');
		
		if ($('.quick-modal-front-modal').length && noOverlay === true) {
			
			// console.log(isClickable);
			
			setTimeout(function(){
				
				$('.quick-modal-front-modal').fadeToggle(1200, function(){
					
					setTimeout(function() {
						
						$('.quick-modal-front-modal .quick-modal-modal-window').on('click', function(e) {
							e.stopPropagation();
						});
						
						$('.quick-modal-front-modal').one('click', function(e) {
								
								if (isClickable) {
									$('.quick-modal-front-modal').fadeToggle(600, function(){
										document.cookie = "saw_modal=1";
									});
								}
								
							
						});
					
					}, 3000);
					
				});
				
			}, 1500);
			
			$('.quick-modal-close').one('click', function(){
				
				$('.quick-modal-front-modal').fadeToggle(600, function(){
					document.cookie = "saw_modal=1";
				});
				
			});
			
		}
		
		// end modal
	
	});
	
})(jQuery);